<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TeamController;

# home
Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('sobre-nos', [AboutController::class, 'index'])->name('about');

Route::get('equipe', [AboutController::class, 'index'])->name('team');
Route::get('equipe/natalia-augusta-ferreira-bordignon', [TeamController::class, 'natalia'])->name('team.natalia');

Route::get('contato', [ContactController::class, 'index'])->name('contact');