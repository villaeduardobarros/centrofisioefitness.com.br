@extends('layouts.app')

@section('content')

<div class="page-title page-title-about bg-pattern" data-bgcolor="55a260">
	<div class="page-title-overlay">
		<div class="container">
			<h1>SOBRE NÓS</h1>
			<p>Nossa clínica cresceu para fornecer um tratamento mais completo aos pacientes</p>
		</div>
	</div>
</div>

<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<ol class="breadcrumb">
				<li class="breadcrumb-home"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
				<li class="active">Sobre nós</li>
			</ol>
		</div>
	</div>
</div>


<div class="history">
	<div class="container">
		<div class="row">

			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 history-images">
				<div class="image-carousel">
					<div class="owl-image-carousel" id="owl-image-carousel">
						<div class="image-carousel-item"> <img src="images/about/about1.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about2.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about3.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about4.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about5.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about6.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about7.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about8.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about9.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about10.png" alt="" /> </div>
						<div class="image-carousel-item"> <img src="images/about/about11.png" alt="" /> </div>
					</div>

					<div class="carousel-btn carousel-next" id="next-image-carousel"><i class="fa fa-angle-right"></i></div>
					<div class="carousel-btn carousel-prev" id="prev-image-carousel"><i class="fa fa-angle-left"></i></div>
				</div>
			</div>

			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 history-text">
				<h3>HISTÓRIA</h3>

				<p>A clínica foi fundada em 2008 pela fisioterapeuta Dra. Natália Augusta Ferreira Bordignon com o objetivo de formar uma equipe multidisciplinar, para que a clínica pudesse oferecer um tratamento mais completo aos pacientes, priorizando as necessidades de cada um. Em 2014 Natália iniciou uma parceria com seu marido e agora sócio Pedro Bordignn Neto, Personal Trainer e a clínica que antes se chamava Centrofisio, passou a se chamar CentroFisio&fitness. Ao mesmo tempo, iniciou-se o trabalho da nutricionista Keila Duarte, e após alguns anos a equipe se estendeu com a entrada da fisioterapeuta neurológica Stefania Fernandes, a Biomédica acupunturista Iani Maito, e a técnica em estética facial e corporal Aline Ribeiro.</p>

				<p>O trabalho em equipe e a atualização profissional, é o foco da clínica, que prioriza as reuniões e discussões de casos, procurando sempre o melhor tratamento para o paciente. A equipe também dispõe de pacotes de tratamento, elaborados a fim de otimizar os resultados. A Centrofisio&fitness promove eventos de promoção a saúde e cursos na área da saúde.</p>

				<p>Áreas de atuação são:</p>
				<ul class="text-list">
					<li>Traumato-Ortopédica</li>
					<li>Uroginecologica e Neurofuncional</li>
					<li>RPG</li>
					<li>Pilates</li>
					<li>Atividade física adaptada a saúde e idosos</li>
					<li>Nutrição clínica e esportiva</li>
					<li>Acupuntura</li>
					<li>Massagens</li>
					<li>Outros tratamentos estéticos.</li>
				</ul>

				<p>A clínica conta com um ambiente adaptado e climatizado, a fim de proporcionar o máximo de conforto aos clientes.</p>
			</div>

		</div>
	</div>
</div>

@endsection