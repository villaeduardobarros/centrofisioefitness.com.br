@extends('layouts.app')

@section('content')

<div class="page-title page-title-about bg-pattern" data-bgcolor="55a260">
	<div class="page-title-overlay">
		<div class="container">
			<h1>NATÁLIA AUGUSTA FERREIRA BORDIGNON</h1>
			<p>Nossa clínica cresceu para fornecer um tratamento mais completo aos pacientes</p>
		</div>
	</div>
</div>

<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<ol class="breadcrumb">
				<li class="breadcrumb-home"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
				<li class="active"><a href="{{ route('team') }}">Equipe</a></li>
				<li class="active">Sobre nós</li>
			</ol>
		</div>
	</div>
</div>


<div class="history">
	<div class="container">
		<div class="row">

			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 history-text">
				<p>Me formei em Fisioterapia em 2006, pela Universidade de França. Desde então, realizei duas especializações, uma em Ortopedia Postural e Traumatologia Desportiva, e a outra em Cardiorrespiratória Geral e Intensiva, além de um aprimoramento em Uroginecologia Clínico-funcional. Me dediquei a estudar Terapia Manual, onde realizei vários cursos internacionais e recebi a certificação internacional da "Manual Concepts" - Austrália. Em 2019 finalizei meu mestrado em Ciências da Saúde, pela FORP - USP. Me identifiquei com a área de DTM (Disfunção Temporomandibular), por ser um conjunto de patologias multifatoriais, e que une um pouco de tudo o que eu já estudei e presenciei em meus atendimentos, possibilitando expandir mais essa área tão complexa e que promete ser uma boa aliada aos tratamentos odontológicos. Hoje em dia, ministro cursos de Fisioterapia, e a possibilidade de unir a minha experiência prática a possibilidade de ensinar e aprender mais, me faz sentir muito grata e realizada.</p>
			</div>

			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 history-images">
				<div class="image-carousel">
					<div class="owl-image-carousel" id="owl-image-carousel">
						<div class="image-carousel-item"> <img src="images/team/natalia-augusta-ferreira-bordignon.jpg" alt="" /> </div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

@endsection