@extends('layouts.app')

@section('content')

<div class="services" id="services"> 
	
	<!-- SERVICES ITEM -->
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 service bg-primary-2">
		<div class="service-icon"> <i class="flaticon-medical-1"></i> </div>
		<div class="service-title">
			<h3>teeth whitening</h3>
		</div>
		<div class="service-text"> Tooth whitening can be a very effective way of lightening the natural colour of your teeth without removing any of the tooth surface. </div>
	</div>
	
	<!-- SERVICES ITEM -->
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 service bg-child-2">
		<div class="service-icon"> <i class="flaticon-medical-2"></i> </div>
		<div class="service-title">
			<h3>implants</h3>
		</div>
		<div class="service-text"> Dental implants may be an option for people who have lost a tooth or teeth due to periodontal disease, an injury, or some other reason. </div>
	</div>
	
	<!-- SERVICES ITEM -->
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 service bg-child-3">
		<div class="service-icon"> <i class="flaticon-tool"></i> </div>
		<div class="service-title">
			<h3>extractions</h3>
		</div>
		<div class="service-text"> In orthodontics if the teeth are crowded, sound teeth may be extracted to create space so the rest of the teeth can be straightened. </div>
	</div>
	
	<!-- SERVICES ITEM -->
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 service bg-child-4">
		<div class="service-icon"> <i class="flaticon-medical"></i> </div>
		<div class="service-title">
			<h3>gum disease</h3>
		</div>
		<div class="service-text"> Plaque is the primary cause of gum disease. However, other factors can contribute to periodontal disease. </div>
	</div>
</div>


<div class="about">
	<div class="container">
		<div class="row"> 

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-text">
				<h2 class="section-title">
					<span class="bold700">SOBRE</span> A CLÍNICA
				</h2>

				<p>O trabalho em equipe e a atualização profissional, é o foco da clínica, que prioriza as reuniões e discussões de casos, procurando sempre o melhor tratamento para o paciente. A equipe também dispõe de pacotes de tratamento, elaborados a fim de otimizar os resultados. A Centrofisio&fitness promove eventos de promoção a saúde e cursos na área da saúde. <a href="{{ route('about') }}">Leia mais &gt;</a></p>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-bg"></div>
		</div>
	</div>
</div>


																
<div class="doctors">
	<div class="container">
		<div class="row">
		
			<h2 class="section-title">
				CONHEÇA <span class="bold700">NOSSA EQUIPE</span>
			</h2>

			<p class="section-subtitle">
				Todos os colaboradores da Centro Fisio & Fitness são profissionais licenciados. Nossa equipe é formada por fisioterapeutas, personal trainer, nutricionista etc.
			</p>

			<div class="doctors-container">
				<div class="owl-doctors" id="owl-doctors"> 

					<div class="doctors-item">
						<div class="doctors-item-container">
							<div class="doctors-item-image">
								<img src="images/team/natalia-augusta-ferreira-bordignon.jpg" alt="Natália Augusta Ferreira Bordignon" />
							</div>
							<div class="doctors-item-name">Natália Augusta Ferreira Bordignon</div>
							<div class="doctors-item-position">Fisioterapeuta</div>
						</div>

						<div class="doctors-item-social">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="#">
								<i class="fa fa-skype"></i>
							</a>
						</div>

						<div class="doctors-item-button">
							<a href="{{ route('team.natalia') }}" class="fancybox-1 btn btn-default">
								<span class="plus">+</span>
								VER MAIS
							</a>
						</div>
					</div>

					<div class="doctors-item">
						<div class="doctors-item-container">
							<div class="doctors-item-image">
								<img src="images/team/pedro-bordignon-neto.jpg" alt="Pedro Bordignon Neto" />
							</div>
							<div class="doctors-item-name">Pedro Bordignon Neto</div>
							<div class="doctors-item-position">Personal Trainer</div>
						</div>

						<div class="doctors-item-social">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="#">
								<i class="fa fa-skype"></i>
							</a>
						</div>

						<div class="doctors-item-button">
							<a href="#bookform2" class="fancybox-1 btn btn-default">
								<span class="plus">+</span>
								VER MAIS
							</a>
						</div>
					</div>

					<div class="doctors-item">
						<div class="doctors-item-container">
							<div class="doctors-item-image">
								<img src="images/team/stefania-m-fernandes.jpg" alt="Stefânia M. Fernandes" />
							</div>
							<div class="doctors-item-name">Stefânia M. Fernandes</div>
							<div class="doctors-item-position">Fisioterapeuta</div>
						</div>

						<div class="doctors-item-social">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="#">
								<i class="fa fa-skype"></i>
							</a>
						</div>

						<div class="doctors-item-button">
							<a href="#bookform2" class="fancybox-1 btn btn-default">
								<span class="plus">+</span>
								VER MAIS
							</a>
						</div>
					</div>

					<div class="doctors-item">
						<div class="doctors-item-container">
							<div class="doctors-item-image">
								<img src="images/team/keila-duarte.jpg" alt="Keila" />
							</div>
							<div class="doctors-item-name">Keila</div>
							<div class="doctors-item-position">Nutricionista</div>
						</div>

						<div class="doctors-item-social">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="#">
								<i class="fa fa-skype"></i>
							</a>
						</div>

						<div class="doctors-item-button">
							<a href="#bookform2" class="fancybox-1 btn btn-default">
								<span class="plus">+</span>
								VER MAIS
							</a>
						</div>
					</div>

					<div class="doctors-item">
						<div class="doctors-item-container">
							<div class="doctors-item-image">
								<img src="images/team/stefania-m-fernandes.jpg" alt="Stefânia M. Fernandes" />
							</div>
							<div class="doctors-item-name">Stefânia M. Fernandes</div>
							<div class="doctors-item-position">Fisioterapeuta</div>
						</div>

						<div class="doctors-item-social">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="#">
								<i class="fa fa-skype"></i>
							</a>
						</div>

						<div class="doctors-item-button">
							<a href="#bookform2" class="fancybox-1 btn btn-default">
								<span class="plus">+</span>
								VER MAIS
							</a>
						</div>
					</div>

				</div>

				<div class="carousel-btn carousel-next" id="next-doctors"><i class="fa fa-angle-right"></i></div>
				<div class="carousel-btn carousel-prev" id="prev-doctors"><i class="fa fa-angle-left"></i></div>
			</div>

		</div>
	</div>
</div>


<div class="numbers" id="numbers">
	<div class="numbers-overlay">
		<div class="container">
			<div class="row">
				<h2 class="section-title">
					<span class="bold700">Dental Clinic</span> in numbers
				</h2>

				<p class="section-subtitle">We can talk for a long time about advantages of our Dental clinic before other medical treatment facilities.<br>
				But you can read the following facts in order to make sure of all pluses of our clinic:</p>

				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 numbers-item">
					<div id="num1" class="numbers-item-number">12</div>
					<div class="numbers-item-title">YEARS<br> OF EXPEREANCE</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 numbers-item">
					<div id="num2" class="numbers-item-number">2405</div>
					<div class="numbers-item-title">HAPPY PATIENTS</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 numbers-item">
					<div id="num3" class="numbers-item-number">84</div>
					<div class="numbers-item-title">QUALIFIED STUFF</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 numbers-item">
					<div id="num4" class="numbers-item-number">67</div>
					<div class="numbers-item-title">MASTER<br>CERTIFICATIONS</div>
				</div>
			</div>
		</div>
	</div>
</div>


			<div class="stories">
				<div class="container">
					<div class="row">
						<h2 class="section-title">
							<span class="bold700">
								SUCCESS
							</span>
							STORIES</h2>
							<p class="section-subtitle">Happy patients stories</p>
							<div class="certs-container">
								<div class="owl-stories" id="owl-stories"> 
									
									<!-- STORIES ITEM -->
									<div class="stories-item">
										<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">
											<div class="stories-item-name"> Jeanette Evvie </div>
											<div class="stories-item-position"> Student </div>
											<div class="stories-item-rating">
												<select id="stories-rating-1" name="stories-rating-1">
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<div class="stories-item-text">
												<div class="stories-item-text-quote"><img src="images/quote.png" alt="" /></div>
												My visits to Dental clinic is always more than just a pleasant experience! I had my cleaning done by Arnie Alban and she did an absolutely impeccable job!! She meticulously and patiently removed the most stubborn stains without causing any discomfort! If you are seeking perfection, then I highly recommend Arnie! </div>
												<div class="stories-item-desc"> Tooth whitening can be a very effective way of lightening the natural colour of your teeth without removing any of the tooth surface.
													<ul>
														<li>Teeth whitening</li>
														<li>Teeth cleaning</li>
													</ul>
												</div>
											</div>
											<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
												<div class="stories-item-before"> <img src="images/stories1.jpg" alt="" />
													<div class="stories-item-before-title"> BEFORE </div>
												</div>
												<div class="stories-item-after"> <img src="images/stories2.jpg" alt="" />
													<div class="stories-item-after-title"> AFTER </div>
												</div>
											</div>
										</div>
										
										<!-- STORIES ITEM -->
										<div class="stories-item">
											<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">
												<div class="stories-item-name"> Candice Heath </div>
												<div class="stories-item-position"> Office Manager </div>
												<div class="stories-item-rating">
													<select id="stories-rating-2" name="stories-rating-2">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>
												<div class="stories-item-text">
													<div class="stories-item-text-quote"><img src="images/quote.png" alt="" /></div>
													I just recently had my braces taken off and to my amazement, underneath was the most beautiful smile I ever could have dreamed possible. I just wanted to thank you for giving me so much to smile about! </div>
													<div class="stories-item-desc"> Orthodontics can give you the straight teeth and proper bite that will help you chew better, smile more often, and keep your teeth longer.
														<ul>
															<li>Orthodontic treatment</li>
															<li>Bite correction</li>
														</ul>
													</div>
												</div>
												<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
													<div class="stories-item-before"> <img src="images/stories3.jpg" alt="" />
														<div class="stories-item-before-title"> BEFORE </div>
													</div>
													<div class="stories-item-after"> <img src="images/stories4.jpg" alt="" />
														<div class="stories-item-after-title"> AFTER </div>
													</div>
												</div>
											</div>
										</div>
										<div class="carousel-btn carousel-next" id="next-stories"><i class="fa fa-angle-right"></i></div>
										<div class="carousel-btn carousel-prev" id="prev-stories"><i class="fa fa-angle-left"></i></div>
									</div>
								</div>
							</div>
						</div>
						<!-- =========================
							END STORIES
							============================== --> 
							
							<!-- =========================
								REVIEW
								============================== -->
								<div class="review">
									<div class="container">
										<div class="row">
											<h2 class="section-title">
												<span class="bold700">
													TESTIMONIALS
												</span>
											</h2>
											<p class="section-subtitle">Many of our patients are our friends now, and they recommend our Dental clinic to their friends and come with their children and<br>
												senior parents to us. We are very proud that you entrust the health of your relatives to us! </p>
												<div class="review-container">
													<div class="owl-review" id="owl-review"> 
														
														<!-- REVIEW ITEM -->
														<div class="review-item">
															<div class="review-item-image"> <img src="images/review1.jpg" alt="" /> </div>
															<div class="review-item-text">This was my first dental treatment in many years and Dr. Callahan made me feel relaxed and calm. It was completely painless and issue free. I thoroughly recommend both Dr. Callahan and Dental clinic. Excellent caring staff! </div>
															<div class="review-item-name">Caetlin Madyson</div>
															<div class="review-item-position">Comprehensive Cleaning</div>
															<div class="review-item-quote"><img src="images/quote.png" alt="" /></div>
														</div>
														
														<!-- REVIEW ITEM -->
														<div class="review-item">
															<div class="review-item-image"> <img src="images/review2.jpg" alt="" /> </div>
															<div class="review-item-text">I would like to say a big thank you to all the team at Dental Clinic, from the charming and efficient reception staff to all the dental team. Dr Wyatt's is a truly first class outfit that are the epitome of professionalism. Satisfied with the service. </div>
															<div class="review-item-name">James Mitchell</div>
															<div class="review-item-position">General Dentistry</div>
															<div class="review-item-quote"><img src="images/quote.png" alt="" /></div>
														</div>
														
														<!-- REVIEW ITEM -->
														<div class="review-item">
															<div class="review-item-image"> <img src="images/review3.jpg" alt="" /> </div>
															<div class="review-item-text">I'm new to Dental Clinic and I have got to say that I am rather impressed with their team's professionalism. I booked online through their website, and I immediately got a call from Diana, their receptionist, confirming my appointment. </div>
															<div class="review-item-name">Tracey Todd</div>
															<div class="review-item-position">Teeth Whitening</div>
															<div class="review-item-quote"><img src="images/quote.png" alt="" /></div>
														</div>
														
														<!-- REVIEW ITEM -->
														<div class="review-item">
															<div class="review-item-image"> <img src="images/review4.jpg" alt="" /> </div>
															<div class="review-item-text">Hands down the best dental clinic I've been to. You'll pay a little more than other clinics but you'll get it right the first time. From the reception to the coffee and down to the dentists; simply awesome! Great customer service. Very satisfied. </div>
															<div class="review-item-name">William Murphy</div>
															<div class="review-item-position">Crown, Root Canal</div>
															<div class="review-item-quote"><img src="images/quote.png" alt="" /></div>
														</div>
													</div>
													<!-- REVIEW BUTTONS -->
													<div class="carousel-btn carousel-next" id="next-review"><i class="fa fa-angle-right"></i></div>
													<div class="carousel-btn carousel-prev" id="prev-review"><i class="fa fa-angle-left"></i></div>
												</div>
											</div>
										</div>
									</div>
									<!-- =========================
										END REVIEW
										============================== --> 
			

										
										<!-- =========================
											TIPS AND FAQ
											============================== -->
											<div class="tipsfaq">
												<div class="container">
																																																		<div class="row"> 
																																																			
																																																			<!-- TIPS -->
																																																			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 tips">
																																																				<h2 class="section-title">
																																																					<span class="bold700">
																																																						TOP TIPS
																																																					</span>
																																																					FROM DOCTORS</h2>
																																																					<!-- TIPS NAVS -->
																																																					<div class="tips-tabs">
																																																						<ul class="nav nav-tabs tabs-carousel">
																																																							<li class="active"><a href="#tips1" data-toggle="tab">Teeth whitening</a></li>
																																																							<li><a href="#tips2" data-toggle="tab">Oral exams</a></li>
																																																							<li><a href="#tips3" data-toggle="tab">Fillings and bridges</a></li>
																																																							<li><a href="#tips4" data-toggle="tab">Root canals</a></li>
																																																							<li><a href="#tips5" data-toggle="tab">Oral exams</a></li>
																																																							<li><a href="#tips6" data-toggle="tab">Root canals</a></li>
																																																						</ul>
																																																						<div class="tips-btn tips-next" id="next-tips"><i class="fa fa-angle-right"></i></div>
																																																						<div class="tips-btn tips-prev" id="prev-tips"><i class="fa fa-angle-left"></i></div>
																																																					</div>
																																																					<!-- TIPS ITEMS -->
																																																					<div class="tips-content">
																																																						<div class="tab-content"> 
																																																							
																																																							<!-- TIPS ITEM -->
																																																							<div class="tab-pane fade active in tips-content-item" id="tips1">
																																																								<div class="tips-content-item-text">
																																																									<p>Tooth whitening (termed tooth bleaching when utilizing bleach), is either restoration of natural tooth shade or whitening beyond natural tooth shade, depending on the definition used.</p>
																																																									<p>Restoration of the underlying, natural tooth shade is possible by simply removing surface (extrinsic) stains (e.g. from tea, coffee, red wine and tobacco) and calculus (tartar). This is achieved by having the teeth cleaned by a dental professional (commonly termed "scale and polish".</p>
																																																									<div class="tips-content-item-btn"> <a href="#" class="btn btn-primary-1">READ MORE</a> </div>
																																																								</div>
																																																								<div class="tips-content-item-image"> <img src="images/blog_author1.jpg" alt="" />
																																																									<div class="tips-content-item-name"> <a href="#">Wyatt Esmond</a> </div>
																																																									<div class="tips-content-item-position"> General Dentist </div>
																																																								</div>
																																																							</div>
																																																							
																																																							<!-- TIPS ITEM -->
																																																							<div class="tab-pane fade tips-content-item" id="tips2">
																																																								<div class="tips-content-item-text">
																																																									<p>Tooth whitening (termed tooth bleaching when utilizing bleach), is either restoration of natural tooth shade or whitening beyond natural tooth shade, depending on the definition used.</p>
																																																									<p>Restoration of the underlying, natural tooth shade is possible by simply removing surface (extrinsic) stains (e.g. from tea, coffee, red wine and tobacco) and calculus (tartar). This is achieved by having the teeth cleaned by a dental professional (commonly termed "scale and polish".</p>
																																																									<div class="tips-content-item-btn"> <a href="#" class="btn btn-primary-1">READ MORE</a> </div>
																																																								</div>
																																																								<div class="tips-content-item-image"> <img src="images/blog_author2.jpg" alt="" />
																																																									<div class="tips-content-item-name"> <a href="#">Arnie Alban</a> </div>
																																																									<div class="tips-content-item-position"> Pedodontics </div>
																																																								</div>
																																																							</div>
																																																							
																																																							<!-- TIPS ITEM -->
																																																							<div class="tab-pane fade tips-content-item" id="tips3">
																																																								<div class="tips-content-item-text">
																																																									<p>Tooth whitening (termed tooth bleaching when utilizing bleach), is either restoration of natural tooth shade or whitening beyond natural tooth shade, depending on the definition used.</p>
																																																									<p>Restoration of the underlying, natural tooth shade is possible by simply removing surface (extrinsic) stains (e.g. from tea, coffee, red wine and tobacco) and calculus (tartar). This is achieved by having the teeth cleaned by a dental professional (commonly termed "scale and polish".</p>
																																																									<div class="tips-content-item-btn"> <a href="#" class="btn btn-primary-1">READ MORE</a> </div>
																																																								</div>
																																																								<div class="tips-content-item-image"> <img src="images/blog_author3.jpg" alt="" />
																																																									<div class="tips-content-item-name"> <a href="#">Dustin Callahan</a> </div>
																																																									<div class="tips-content-item-position"> Orthodontist </div>
																																																								</div>
																																																							</div>
																																																							
																																																							<!-- TIPS ITEM -->
																																																							<div class="tab-pane fade tips-content-item" id="tips4">
																																																								<div class="tips-content-item-text">
																																																									<p>Tooth whitening (termed tooth bleaching when utilizing bleach), is either restoration of natural tooth shade or whitening beyond natural tooth shade, depending on the definition used.</p>
																																																									<p>Restoration of the underlying, natural tooth shade is possible by simply removing surface (extrinsic) stains (e.g. from tea, coffee, red wine and tobacco) and calculus (tartar). This is achieved by having the teeth cleaned by a dental professional (commonly termed "scale and polish".</p>
																																																									<div class="tips-content-item-btn"> <a href="#" class="btn btn-primary-1">READ MORE</a> </div>
																																																								</div>
																																																								<div class="tips-content-item-image"> <img src="images/blog_author4.jpg" alt="" />
																																																									<div class="tips-content-item-name"> <a href="#">Kristin Weaver</a> </div>
																																																									<div class="tips-content-item-position"> Hygienist </div>
																																																								</div>
																																																							</div>
																																																							
																																																							<!-- TIPS ITEM -->
																																																							<div class="tab-pane fade tips-content-item" id="tips5">
																																																								<div class="tips-content-item-text">
																																																									<p>Tooth whitening (termed tooth bleaching when utilizing bleach), is either restoration of natural tooth shade or whitening beyond natural tooth shade, depending on the definition used.</p>
																																																									<p>Restoration of the underlying, natural tooth shade is possible by simply removing surface (extrinsic) stains (e.g. from tea, coffee, red wine and tobacco) and calculus (tartar). This is achieved by having the teeth cleaned by a dental professional (commonly termed "scale and polish".</p>
																																																									<div class="tips-content-item-btn"> <a href="#" class="btn btn-primary-1">READ MORE</a> </div>
																																																								</div>
																																																								<div class="tips-content-item-image"> <img src="images/blog_author5.jpg" alt="" />
																																																									<div class="tips-content-item-name"> <a href="#">Leslie Adams</a> </div>
																																																									<div class="tips-content-item-position"> Prosthodontics </div>
																																																								</div>
																																																							</div>
																																																							
																																																							<!-- TIPS ITEM -->
																																																							<div class="tab-pane fade tips-content-item" id="tips6">
																																																								<div class="tips-content-item-text">
																																																									<p>Tooth whitening (termed tooth bleaching when utilizing bleach), is either restoration of natural tooth shade or whitening beyond natural tooth shade, depending on the definition used.</p>
																																																									<p>Restoration of the underlying, natural tooth shade is possible by simply removing surface (extrinsic) stains (e.g. from tea, coffee, red wine and tobacco) and calculus (tartar). This is achieved by having the teeth cleaned by a dental professional (commonly termed "scale and polish".</p>
																																																									<div class="tips-content-item-btn"> <a href="#" class="btn btn-primary-1">READ MORE</a> </div>
																																																								</div>
																																																								<div class="tips-content-item-image"> <img src="images/blog_author6.jpg" alt="" />
																																																									<div class="tips-content-item-name"> <a href="#">Helen Edwards</a> </div>
																																																									<div class="tips-content-item-position"> Pediatric Dentistry </div>
																																																								</div>
																																																							</div>
																																																						</div>
																																																					</div>
																																																				</div>
																																																				
																																																				<!-- FAQ -->
																																																				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 faq">
																																																					<h2 class="section-title">
																																																						<span class="bold700">
																																																							FAQ
																																																						</span>
																																																					</h2>
																																																					<div class="panel-group" id="accordion"> 
																																																						<!-- FAQ ITEM -->
																																																						<div class="panel panel-default">
																																																							<div class="panel-heading">
																																																								<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <i class="fa fa-heartbeat"></i> Surgical implant placements </a> </h4>
																																																							</div>
																																																							<div id="collapseOne" class="panel-collapse collapse in">
																																																								<div class="panel-body"> Dental implant placement is no longer performed only by oral surgeons and periodontists; general dentists are also increasingly providing difficult surgical implant services. Dental implants may be used to replace single teeth, replace multiple teeth, or provide abutments for complete dentures or partials. This topic focuses on the placement of single-tooth dental implants. </div>
																																																							</div>
																																																						</div>
																																																						
																																																						<!-- FAQ ITEM -->
																																																						<div class="panel panel-default">
																																																							<div class="panel-heading">
																																																								<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed"> <i class="fa fa-medkit"></i> Teeth whitening </a> </h4>
																																																							</div>
																																																							<div id="collapseTwo" class="panel-collapse collapse">
																																																								<div class="panel-body"> Dental implant placement is no longer performed only by oral surgeons and periodontists; general dentists are also increasingly providing difficult surgical implant services. Dental implants may be used to replace single teeth, replace multiple teeth, or provide abutments for complete dentures or partials. This topic focuses on the placement of single-tooth dental implants. </div>
																																																							</div>
																																																						</div>
																																																						
																																																						<!-- FAQ ITEM -->
																																																						<div class="panel panel-default">
																																																							<div class="panel-heading">
																																																								<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed"> <i class="fa fa-life-bouy"></i> Teeth cleaning </a> </h4>
																																																							</div>
																																																							<div id="collapseThree" class="panel-collapse collapse">
																																																								<div class="panel-body"> Dental implant placement is no longer performed only by oral surgeons and periodontists; general dentists are also increasingly providing difficult surgical implant services. Dental implants may be used to replace single teeth, replace multiple teeth, or provide abutments for complete dentures or partials. This topic focuses on the placement of single-tooth dental implants. </div>
																																																							</div>
																																																						</div>
																																																						
																																																						<!-- FAQ ITEM -->
																																																						<div class="panel panel-default">
																																																							<div class="panel-heading">
																																																								<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed"> <i class="fa fa-flask"></i> X-rays </a> </h4>
																																																							</div>
																																																							<div id="collapseFour" class="panel-collapse collapse">
																																																								<div class="panel-body"> Dental implant placement is no longer performed only by oral surgeons and periodontists; general dentists are also increasingly providing difficult surgical implant services. Dental implants may be used to replace single teeth, replace multiple teeth, or provide abutments for complete dentures or partials. This topic focuses on the placement of single-tooth dental implants. </div>
																																																							</div>
																																																						</div>
																																																						
																																																						<!-- FAQ ITEM -->
																																																						<div class="panel panel-default">
																																																							<div class="panel-heading">
																																																								<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed"> <i class="fa fa-ambulance"></i> Crowns </a> </h4>
																																																							</div>
																																																							<div id="collapseFive" class="panel-collapse collapse">
																																																								<div class="panel-body"> Dental implant placement is no longer performed only by oral surgeons and periodontists; general dentists are also increasingly providing difficult surgical implant services. Dental implants may be used to replace single teeth, replace multiple teeth, or provide abutments for complete dentures or partials. This topic focuses on the placement of single-tooth dental implants. </div>
																																																							</div>
																																																						</div>
																																																					</div>
																																																				</div>
																																																			</div>
																																																		</div>
																																																	</div>
																																																	<!-- =========================
																																																		END TIPS AND FAQ
																																																		============================== --> 
																																																		
																																																		<!-- =========================
																																																			CLIENTS
																																																			============================== -->
																																																			<div class="clients">
																																																				<div class="container">
																																																					<div class="row">
																																																						<p class="section-subtitle">PARTNER COMPANIES</p>
																																																						<div class="clients-container">
																																																							<div class="owl-clients" id="owl-clients"> 
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients1.jpg" alt="" /> </div>
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients2.jpg" alt="" /> </div>
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients3.jpg" alt="" /> </div>
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients4.jpg" alt="" /> </div>
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients5.jpg" alt="" /> </div>
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients2.jpg" alt="" /> </div>
																																																								
																																																								<!-- CLIENTS ITEM -->
																																																								<div class="clients-item"> <img src="images/clients3.jpg" alt="" /> </div>
																																																							</div>
																																																							
																																																							<!-- CLIENTS BUTTONS -->
																																																							<div class="carousel-btn carousel-next" id="next-clients"><i class="fa fa-angle-right"></i></div>
																																																							<div class="carousel-btn carousel-prev" id="prev-clients"><i class="fa fa-angle-left"></i></div>
																																																						</div>
																																																					</div>
																																																				</div>
																																																			</div>

@endsection


																																																
																																																																														