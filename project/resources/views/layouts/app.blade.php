<?php
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	setlocale(LC_TIME, "pt_BR", "pt_BR.utf-8", "pt_BR.utf-8", "portuguese");
	date_default_timezone_set("America/Sao_Paulo");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{ URL::to('/') }}">
	<title>{{ env('APP_NAME') }}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="description" content="Creative Responsive Bootstrap Multi-Purpose HTML Template">
	<meta name="keywords" content="H2O, Creative, Agency, Portfolio, Freelancer, Shop, Store,Bootstrap, HTML, Template, One page">
	<meta name="author" content="Vadzim Liashkevich">
	<meta name="robots" content="index, follow" />
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<link rel="canonical" href="{{ URL::current() }}"/>
	<link rel="shortcut icon" href="images/favicon/favicon.ico">	
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
	<link rel="manifest" href="images/favicon/site.webmanifest">
	<link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="stylesheet" type="text/css" href="css/master.css" />

	

	<!-- meta tags -->
	<meta name="description" content="Faça já seu catalogo de produtos online e disponibilize para seus clientes façam pedidos direto no whatsapp, sem intermediários." /> <!-- na single pegar automatico descricao do estabelecimento 160caracteres, nas demais seria bom padronizar e so mudar se ta no cadastro, sobre etc, se quiser por no admin melhor ainda -->
	
		<meta property="og:locale" content="pt_BR">

	<meta property="og:url" content="{{ URL::current() }}">

	<meta property="og:title" content="{{ env('APP_NAME') }}"> <!-- na single pegar automatico: nome do local - categoria - cidade-uf - suplist.com.br -->
	<meta property="og:site_name" content="SUPList - Seu catalogo de produtos online">
	<meta property="og:description" content="Faça já seu catalogo de produtos online e disponibilize para seus clientes façam pedidos direto no whatsapp, sem intermediários."> <!-- na single pegar automatico descricao do estabelecimento 160caracteres, nas demais seria bom padronizar e so mudar se ta no cadastro, sobre etc, se quiser por no admin melhor ainda -->

	<meta property="og:image" content="www.meusite.com.br/imagem.jpg"> <!-- logo do estabelecimento -->
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="768">
	<meta property="og:image:height" content="768">
	<meta property="og:type" content="website">
	<!-- meta tags -->

	<?php # exibe CSS controller ?>
	@if ($arrCss)
		@foreach ($arrCss as $css)
			{!! HTML::style($css) !!}
		@endforeach
	@endif

	<script src="js/jquery-2.2.0.min.js"></script> 
	<script>var baseUrl = '{{ URL::to('/') }}';</script>

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body data-spy="scroll" data-target=".navbar">

	<?php # header ?>
	@include('layouts.header')

	<?php # conteudo ?>
	@yield('content')

	<div class="get-modal"></div>

	<?php # header ?>
	@include('layouts.footer')

	<?php # standard modal ?>
	<div id="getModal"></div>

	<?php # exibe JS controller ?>
	@if ($arrJs)
		@foreach ($arrJs as $js)
			{!! HTML::script($js) !!}
		@endforeach
	@endif

</body>
</html>
