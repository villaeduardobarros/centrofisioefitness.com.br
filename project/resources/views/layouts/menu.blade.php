<ul class="nav navbar-nav">
	<li class="{{ (($page == 'home') ? 'active' : NULL) }}">
		<a href="/">HOME</a>
	</li>

	<li class="{{ (($page == 'about') ? 'active' : NULL) }}">
		<a href="{{ route('about') }}">SOBRE NÓS</a>
	</li>

	<li class="dropdown">
		<a data-toggle="dropdown" href="#">EQUIPE</a>
		
		<ul class="dropdown-menu" role="menu">
			<li><a href="{{ route('team.natalia') }}">NATÁLIA AUGUSTA FERREIRA BORDIGNON</a></li>
		</ul>
	</li>

	<li class="{{ (($page == 'contact') ? 'active' : NULL) }}">
		<a href="contato">CONTATO</a>
	</li>
</ul>