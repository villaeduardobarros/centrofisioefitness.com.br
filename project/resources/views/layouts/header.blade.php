<div class="preloader" id="preloader">
	<img src="images/preloader.gif" alt="" />
</div>

<div class="top-bar" id="top-bar">
	<div class="container">
		<div class="row"> 
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<div class="top-bar-adress">
					<i class="flaticon-navigation-arrow"></i> Rua São Paulo, 1306 - São Joaquim da Barra, SP 14600-000
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<div class="top-bar-social">
					<a href="https://www.facebook.com/centrofisiofitness/">
						<i class="fa fa-facebook"></i>
					</a>
					
					<a href="#">
						<i class="fa fa-instagram"></i>
					</a>
					
					<a href="#">
						<i class="fa fa-twitter"></i>
					</a>
					
					<a href="#">
						<i class="fa fa-skype"></i>
					</a>
				</div>
				
				<div class="top-bar-mail">
					<i class="fa fa-envelope"></i> contato@centrofisioefitness.com.br
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="header" id="header">
	<div class="container">
		<div class="row"> 
			
			<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
				<div class="header-logo">
					<a href="/">
						<img src="images/logo.png" alt="Centro Fisio & Fitness" style="width:220px;" />
					</a>
				</div>
			</div>
			
			<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
				<div class="header-button">
					<a href="https://api.whatsapp.com/send?phone=5516988182392" class="btn btn-primary">
						<i class="fa fa-whatsapp"></i> 16 98818.2392
					</a>
				</div>
				
				<div class="header-phone">
					<i class="fa fa-phone"></i> 16 3728.2392
				</div>
			</div>

		</div>
	</div>
</div>

<div id="smartposition"></div>

<div class="top-menu" id="top-menu">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="mobile-bar-cont">
					
					<div class="top-menu-logo">
						<a href="/">
							<img src="images/logo.png" alt="Centro Fisio & Fitness" style="height:44px !important;margin:4px 0;width:162px !important;" />
						</a>
					</div>
					
					<div class="mobile-bar">
						<div class="show-menu" id="show-menu">
							<i class="fa fa-bars"></i>
						</div>
						
						<div class="logo-for-mobile">
							<a href="#" title="#">
								<img src="images/logo_m.png" alt="Centro Fisio & Fitness">
							</a>
						</div>
					</div>
					
				</div>
				
				@include('layouts.menu')
				
			</div>
		</div>
	</div>
</div>

@if ($banner)

	<div id="slider" class="slider">
		<div class="sp-slides"> 

			<div class="sp-slide"> <img class="sp-image" src="css/images/blank.gif" alt="" data-src="images/banners/banner1.png" />
				{{-- <div class="container">
					<h3 class="sp-layer slider-welcome" data-position="leftCenter" data-horizontal="15" data-vertical="-370" data-show-transition="left" data-hide-transition="left" data-show-delay="0" data-hide-delay="0"> Welcome Dental Clinic </h3>

					<h1 class="sp-layer slider-title" data-position="leftCenter" data-horizontal="15" data-vertical="-230" data-show-transition="left" data-hide-transition="left" data-show-delay="200" data-hide-delay="200"> It is quite simple! </h1>

					<h2 class="sp-layer slider-subtitle" data-position="leftCenter" data-horizontal="15" data-vertical="-120" data-show-transition="left" data-hide-transition="left" data-show-delay="400" data-hide-delay="400"> You choose convenient time! </h2>

					<p class="sp-layer slider-text" data-position="leftCenter" data-horizontal="15" data-vertical="80" data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="600"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br>incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos-<br>trud exercitation ullamco laboris. </p>

					<p class="sp-layer slider-button" data-position="leftCenter" data-horizontal="15" data-vertical="300" data-show-transition="left" data-hide-transition="left" data-show-delay="800" data-hide-delay="800"> <a href="#" class="btn btn-primary">READ MORE</a> </p>
				</div> --}}
			</div>

		</div>
	</div>

@endif