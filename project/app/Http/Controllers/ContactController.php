<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{

	protected $arrCss;
	protected $arrJs;

	public function __construct()
	{
		$this->arrCss = NULL;
		$this->arrJs = array(
			'js/bootstrap.min.js',
			'js/jquery.sliderPro.min.js',
			'js/jquery.fancybox.pack.js',
			'js/owl.carousel.js',
			'js/jquery.barrating.min.js',
			'js/isotope.pkgd.min.js',
			'js/scrollspy.js',
			'js/moment.js',
			'js/bootstrap-datetimepicker.min.js',
			'js/jquery.form.js',
			'js/jquery.form-validator.js',
			'js/jquery.selectBox.js',
			'js/jquery.matchHeight.js',
			'js/imagesloaded.pkgd.min.js',
			'js/theme.js',
	);
	}

	public function index()
	{
		return view('contact.contact', [
			'arrCss'	=> $this->arrCss,
			'arrJs'		=> $this->arrJs,
			'page'		=> 'contact',
			'banner'	=> FALSE
		]);
	}
}